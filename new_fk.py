import math

class triginometricalRandomNumberGenerator:
    def __init__(self,seed):
        self.__seed = seed
        self.__sequence = []
        self.__member = 0
        self.__valueOfMembers = 0
        self.__notWasCalled = True
    def next(self):
        if self.__notWasCalled:
            self.__valueOfMembers += 1
            m = ((self.__seed * int(math.sin(self.__seed)+2) + self.__seed * int(math.cos(self.__seed)+2))**4) // (self.__seed**2+1)
            self.__sequence.append(m)
            self.__notWasCalled = False
            return m
        else:
            m = ((self.__sequence[self.__valueOfMembers-1] * int(math.sin(self.__sequence[self.__valueOfMembers-1])+2) + self.__sequence[self.__valueOfMembers-1] * int(math.cos(self.__sequence[self.__valueOfMembers-1])+2))**4) // (self.__sequence[self.__valueOfMembers-1]**2+1)
            self.__sequence.append(m)
            self.__valueOfMembers += 1
            return m
    def getSequence(self):
        return self.__sequence
    def getSeed(self):
        return self.__seed

seed = int(input())
gen = triginometricalRandomNumberGenerator(seed)
n = int(input())
for i in range(n):
    print(gen.next())
print(gen.getSequence())
        

