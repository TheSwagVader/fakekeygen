import hashlib, random, string, os, math, datetime, random
def generateRandomString(size):
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(size))

def cls():
    os.system('cls' if os.name == 'nt' else 'clear')

def genWinKey():
    return f'{generateRandomString(5)}-{generateRandomString(5)}-{generateRandomString(5)}-{generateRandomString(5)}-{generateRandomString(5)}'

def genAutodeskKey():
    key = ''
    for i in range(5):
        key += (genWinKey() + '\n')
    return key

def genPlutoniumKey(req):
    #k = int(((req**4)/(req**2+req**3)) * math.sin(req))
    #m = int((math.cos(req)**k+math.sin(req)**k)/(req-k))
    #n = int((req**(m+k))/(m+k))
    #req = (req - k)*m+n
    prep, key, confirm = '', '', ''
    for i in range(req, 2*req+1):
        prep += hashlib.sha512(str(i).encode()).hexdigest()
    key = prep[req//2:req//2+128]
    confirm = prep[req-64:req+64]
    return (key, confirm)

def genUraniumKey(req):
    prep, key, confirm = '', '', []
    for i in range(req, req**2, req):
        prep += hashlib.sha512(str(i).encode()).hexdigest()
    now = datetime.datetime.now().strftime('%d-%m-%Y-%H-%M-%S')
    prep += hashlib.sha512(now.encode()).hexdigest()
    nowNumber, prepLen = int(now, 16), len(prep)
    start = (prepLen+req) // 8
    secondStart = (req + prepLen) // (req**2)
    key = prep[start:start+128]
    for i in range(3):
        confirm.append(prep[i*secondStart-64+req:i*secondStart+64+req])
    return (key, (confirm[0], confirm[1], confirm[2]))

def genRanQKey(req1,req2):
    m = req1 * req2
    prep = ''.join([hashlib.sha512(str(i).encode()).hexdigest() for i in range(m-req1,m+req2+1)])
    s = random.randint(req1,req2)
    return prep[s:s+128]

def genNRanQKey(req1,req2):
    m, n, k = req1 * req2, (req1+req2)//2, int(math.sqrt(req1*req2))
    prep = ''.join([hashlib.sha512(str(i).encode()).hexdigest() for i in range(m-req1,m+req2+1)])
    s = n - k
    return prep[s:s+128]

def genChaosChildKey():
    ps = generateRandomString(16)
    pk = int(bytearray(ps.encode('utf-8')).hex(),16)
    a, b, c, d, e, f, g, h = int(math.sqrt(pk)), int(math.sin(pk)), int(math.cos(pk)), int(math.sinh(pk)), int(math.cosh(pk)), int(math.log(pk, math.e)), int(pk**2), int(math.exp(pk))
    bk = a + b + c + d + e + f + g + h
    fk = a * b * c * d * e * f * g * h
    prep = ''.join([hashlib.sha512(str(i).encode()).hexdigest() for i in range(fk-bk,fk+bk+1)])
    return prep[:128]


cls()
while True:
    print('''
FakeKeygen - keygen for fun
1. Generate win-like key
2. Generate Autodesk-like key
3. Generate Plutonium key (unreal)
4. Generate Uranium key (unreal)
5. Generate RanQ key (unreal)
6. Generate NRanQ key (unreal)
7. Generate ChaosChild key (unreal)
8. Exit''')
    choice = int(input('>'))
    if choice == 1:
        print(f'Generated key: {genWinKey()}')
        input()
        cls()
    elif choice == 2:
        print(f'Generated key:\n{genAutodeskKey()}')
        input()
        cls()
    elif choice == 3:
        req = int(input('Enter request code: '))
        key = genPlutoniumKey(req)
        print(f'Generated key: {key[0]}\nConfirm code: {key[1]}')
        input()
        cls()
    
    elif choice == 4:
        req = int(input('Enter request code: '))
        key = genPlutoniumKey(req)
        print(f'Main key: {key[0]}\nPrimary confirm code: {key[1][0]}\nSecondary confirm code: {key[1][1]}\nThirdary confirm code: {key[1][2]}')
        input()
        cls()
    elif choice == 5:
        req1 = int(input('Enter first request code: '))
        req2 = int(input('Enter second request code: '))
        key = genRanQKey(req1,req2)
        print(f'Generated key: {key}')
        input()
        cls()
    elif choice == 6:
        req1 = int(input('Enter first request code: '))
        req2 = int(input('Enter second request code: '))
        key = genNRanQKey(req1,req2)
        print(f'Generated key: {key}')
        input()
        cls()
    if choice == 7:
        print(f'Generated key: {genChaosChildKey()}')
        input()
        cls()
    elif choice == 8:
        break
    cls()